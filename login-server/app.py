from flask import Flask, send_from_directory, request,render_template, session
from server.login_form import LoginForm, RegistrationForm, serchForm
from flask_login import login_required,login_user
from server.data_manager import data
from server.user_manager import *
from email import message
from click import confirm
from ast import Break
import flask
import os

app = Flask(__name__,static_url_path='/static')

app.secret_key = os.urandom(24)
app.register_blueprint(data)

login_manager.init_app(app)
login_manager.login_view = "login"

@app.route('/',methods=['GET', 'POST'])
@app.route('/index.html',methods=['GET', 'POST'])
@login_required
def index_html():
    form = serchForm()

    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)

    if form.validate_on_submit():
        target = form.username.data
        print("aa")
        if target:
            target = hash(target)
            if target in get_users() and target != session.get('user'):
                session['target'] = target
                return flask.redirect(flask.url_for('chat_html'))

    return render_template('index.html', user=session.get('user'), form=form)



@app.route('/login', methods=['GET', 'POST'])
def login():

    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)

    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        u = vertify_user(username,password)
        if u: # and check_password(u.password, password):
            user = user_loader(u)
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            
            print(flask.url_for('index_html'))
    
            if False and not is_safe_url(next):
                return flask.abort(400)


            session['user'] = user.get_id()
            return flask.redirect(flask.url_for('index_html'))

    return render_template('login.html', form=form)


@app.route('/registration', methods=['GET', 'POST'])
def registration():

    form = RegistrationForm()

    if form.is_submitted():
         print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
         print(request.form)

    if form.validate_on_submit():

        username = form.username.data
        password = form.password.data
        if add_user(username, password):
            flask.flash('Thanks for registering')
            return flask.redirect(flask.url_for('login'))
    
    return render_template('registration.html', form=form)

@app.route('/chat.html')
@login_required
def chat_html():
    return render_template('chat.html', user=session.get('user'),target=session.get('target'))



if __name__ == "__main__":
    app.run("127.0.0.1",5000,debug=True)