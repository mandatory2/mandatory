

var chatBody = document.getElementById('chatBody');


var autoCheck = async (user,receiver) => {
    search(user,receiver)
    setInterval( () => {search(user,receiver)},
        3000
    )
}



var search = async (user,receiver) => {
    const q = `/search?a=${encodeURIComponent(user)}&b=${encodeURIComponent(receiver)}`;
    res = await fetch(q);
    content = JSON.parse(await res.text());
    console.log(content);

    const elts = [];
    content.forEach((element) => {
        let node = document.createElement('div');
        node.className = element.at(1) == user ? 'msg me' : 'msg';

        let timeStamp = document.createElement('div');
        timeStamp.className = 'time';
        timeStamp.innerHTML = element.at(4);
        node.appendChild(timeStamp);

        let text = document.createElement('div');
        text.className = 'msg-text';
        text.innerHTML = element.at(3);
        node.appendChild(text);

        elts.push(node);
    })
    chatBody.replaceChildren(...elts); 
};

var messageField = document.getElementById('message');
var send = async (sender,receiver) => {
    const q = `/send?sender=${encodeURIComponent(sender)}&receiver=${receiver}&message=${encodeURIComponent(messageField.value)}`;
    res = await fetch(q, { method: 'post' });
    console.log(res);
    messageField.value = "";
    
    search(sender,receiver);
};
