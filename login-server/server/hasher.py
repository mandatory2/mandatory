from hashlib import sha3_512

def hash(string:str) -> str:
    return sha3_512(string.encode('utf-8'),usedforsecurity=True).hexdigest()

def equals(hash1:str, hash2:str) -> bool:
    return hash1 == hash2

