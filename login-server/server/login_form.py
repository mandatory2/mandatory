from wtforms import StringField, PasswordField, SubmitField, validators
from flask_wtf import FlaskForm


class serchForm(FlaskForm):
    username = StringField('Username')
    submit = SubmitField('Submit')

class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')

class RegistrationForm(FlaskForm):
    username = StringField('Username', [validators.Length(min=4, max=256)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.length(min=8, max=256),
        validators.EqualTo('re_password', message='Passwords must match')
    ])
    re_password = PasswordField('Re_Password', [
        validators.DataRequired(),
        validators.length(min=8, max=256)
    ])
    submit = SubmitField('Submit')

