from datetime import datetime
from flask import Blueprint,request
from server.user_manager import get_users
from server.pygmenter import pygmentize
from json import dumps
from apsw import Error
import apsw
import sys
import html # Input Sanitization

try:
    conn = apsw.Connection('./login-server/server/tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id INTEGER PRIMARY KEY, 
        sender TEXT NOT NULL,
        receiver TECT NOT NULL,
        message TEXT NOT NULL,
        timestamp TEXT NOT NULL);''')

except Error as e:
    print(e)
    sys.exit(1)


data = Blueprint('data_print',__name__)


@data.get('/search')
def search():
    query = html.escape(request.args.get('q') or request.form.get('q') or '*') # Sanitized
    stmt = html.escape(f"SELECT * FROM messages WHERE message GLOB '{query}'") # Sanitized

    sender = html.escape(request.args.get('a')) # Sanitized
    receiver = html.escape(request.args.get('b')) # Sanitized

    stmt = f"SELECT * FROM messages WHERE (receiver is '{receiver}' AND sender is '{sender}') or (sender is '{receiver}' AND receiver is '{sender}')"
    result = f"Query: {pygmentize(stmt)}\n"

    try:
        c = conn.execute(stmt)
        rows = c.fetchall()
        c.close()
        return dumps(rows)
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@data.route('/send', methods=['POST','GET'])
def send():
    try:
        sender = html.escape(request.args.get('sender') or request.form.get('sender')) # Sanitized
        receiver = html.escape(request.args.get('receiver') or request.form.get('receiver')) # Sanitized
        message = html.escape(request.args.get('message') or request.args.get('message')) # Sanitized

        if not sender or sender not in get_users():
            return f'ERROR: Missing sender!'
        if not message:
            return f'ERROR: Missing message! Cannot send empty message!'

        stmt = html.escape(f"INSERT INTO messages (sender, message) values ('{sender}', '{message}');") # Sanitized
        time = datetime.now()
        time_str = time.strftime("%d/%m/%Y %H:%M:%S")

        stmt = f"INSERT INTO messages (sender,receiver,message,timestamp) values ('{sender}', '{receiver}', '{message}', '{time_str}');"
        result = f"Query: {pygmentize(stmt)}\n"
        conn.execute(stmt)
        return f'{time_str} - Sent: {message}'
    except Error as e:
        return f'{result}ERROR: {e}'


