from flask_login import UserMixin
from server.hasher import hash
from apsw import Error
import flask_login
import random
import string
import apsw
import sys
import os

import html

class User(UserMixin):
    """
    Class to store user info
    UserMixin provides us with an `id` field and the necessary
    methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
    """
    def __init__(self,id) -> None:
        super().__init__()
        self.id = id
    pass

try:
    conn = apsw.Connection('./login-server/server/users.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL,
        password TEXT NOT NULL,
        token TEXT NOT NULL);''')
except Error as e:
    print(e)
    sys.exit(1)

def generate_token(length):
    letters = string.ascii_letters + string.digits
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

# safe method (https://realpython.com/prevent-python-sql-injection/#using-query-parameters-in-sql)
# cursor.execute("""SELECT admin FROM users WHERE username = %(username)s """, { 'username': username })

def add_user(username, password, token=generate_token(16)):
    
    u_name = hash(html.escape(username)) # Sanitized
    p_word = hash(html.escape(password)) # Sanitized
    token = hash(html.escape(token)) # Sanitized

    stmt = f"INSERT INTO users (username, password, token) values ('{u_name}', '{p_word}', '{token}');"
    conn.execute(stmt)

    print(f"ADDED USER : {username}")
    return True


def get_users() -> list:
    stmt = f"SELECT username,password FROM users;"
    c = conn.execute(stmt)
    out = {}
    for row in c:
        out[row[0]] = row[1]
    return out

def vertify_user(username:str, password:str) -> str:
    users = get_users()
    u_name = hash(username)
    p_word = hash(password)

    if u_name in users:
        if users[u_name] == p_word:
            return u_name
    return None

login_manager = flask_login.LoginManager()

@login_manager.user_loader
def user_loader(user_id):
    """
    This method is called whenever the login manager needs to get
    the User object for a given user id
    """
    if user_id not in get_users(): return

    user = User(user_id)
    return user

@login_manager.request_loader
def request_loader(request):
    """
    This method is called to get a User object based on a request,
    for example, if using an api key or authentication token rather
    than getting the user name the standard way (from the session cookie)
    """
    auth = request.headers.get('Authorization')
    if not auth: return