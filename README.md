# INF 226 - mandatory 2+3
#### Lars, Jone and Gard
***

### Task 1

Since the code was mixed into one file, it was hard to find the structure of the code. 
Bob's and Alice's password was in a dictionary together with their tokens. 
We could see the secret token on the website. The webpage could be very easily SQL injected
through the search method, we could get easily get the whole database and its contents. 
If an attacker got accesses to app.py file, since everything was in that one file, the attacker 
would have had access to everything.

### Our project

To run the project you will need to have downloaded ``flask, flask_wtf, flask_login, apsw, pygments`` you can get these
by using ``pip install``. Then in the terminal/cmd you can run the app.py by typing ``flask run``, the server will now
start in debugging mode. 

There is already two users on the server Bob and Alice, you can log in to these if 
you dont want to make your own profile. Bobs password is "bananas" and Alice's is : "password123', you can use these profiles.

We know that we could use wireshark to see everything that goes on in our website, such as 
password hijacking, but we could have fixed this by using https instead of http.

We have structured the project into three different "folder" the [server side](login-server/server)
where all the scripts and the handlers to the server takes place. So everything that has something to do with the
server is in that folder. 

Then the [static](login-server/static) folder, where all the [css](login-server/static/css) files to the different websites, [images](login-server/static/img) and the [scripts](login-server/static/scripts) to the website is located
this together with the [templates](login-server/templates) is the design to the website and
how it operates to the user. The serverside folder is the backend and the templates and static is frontend

The project is a simple message website, you can send and receive messages with as many people that you like. 
You can create your own profile and chat with people on the website. 

### Questions

+ _Threat model_
  + _Who might attack the application?_
    + People who would like to get sensitive information about the users
    and wants to see the messages between users.
  + _What can an attacker do?_
    + Try to steal the information about the users, either by phishing, bruteforce etc
  + _What damage could be done (in terms of confidentiality, integrity, availability)?_
    + Confidentiality, could leak messages if access was gained. Integrity, 
    could pose as others if access was gained. Availability, could DOS and DDOS the site, so it would not be available.
  + _Are there limits to what an attacker can do?_
    + Yes there is limits to what the attacker can do. The attacker cant use SQL injection or HTML injection. neither wireshark to get the information about the user
  + _Are there limits to what we can sensibly protect against?_
    + Yes, we can't protect against everything, then we wouldn't have an application that is "user friendly"
      . The only safe application there is an application that don't have aby availability. Since we want 
    the users to be able to use the website, the security has to "go down"
  
      

+ _What are the main attack vectors for the application?_
  + The main attack vectors would be:
    + Weak Credentials 
    + Brute Force
    + DDOS
    + Cross-site Scripting
    + Man in the middle attacks

+ _What should we do (or what have you done) to protect against attacks?_
  + One thing that could improve security is to give the user as little access as possible, while
  maintaining usability 
  + Using encryption's on the messages we send to each other
  + We should use a program that checks when an attack is happening
  + We have implemented some HTML and SQL injection preventions. 
+ _What is the access control model?_
  + That is the term used for the different access control models. Like DAC and MAC.
+ _How can you know that your security is good enough? (traceability)_
  + If an attacker cannot get access to the website, and if they do get access, the damage they can do
  is limited. 
  + When an attacker attacks we should be able to see where the attack came from, e.g trace their route